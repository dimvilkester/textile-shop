<?php 
    require_once 'catalog.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Test application</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <link rel="stylesheet" href="_assets/module/normalize.css" />
        <link rel="stylesheet" href="_assets/css/style.css" />
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="wrapper">           
            <header class="header">
                <div class="header__inner inner">
                    <div class="header-logo">
                        <div class="header-logo__image">
                            <a href="index.html" class="header-logo__link">
                                <img src="_assets/img/style/logo.png" alt="Enteley" class="header-logo__img"/>
                            </a>
                        </div>
                    </div>

                    <nav class="header__menu">
                        <ul class="header__menu-inner">
                            <li class="header__menu-item">
                                <a class="header__menu-link" href="about.html">О компании</a>
                            </li>
                            <li class="header__menu-item">
                                <a class="header__menu-link" href="index.html">Каталог товаров</a>
                            </li>
                            <li class="header__menu-item">
                                <a class="header__menu-link" href="pravila.html">Памятка покупателю</a>
                            </li>
                            <li class="header__menu-item">
                                <a class="header__menu-link" href="oplata-dostavka.html">Оплата и доставка</a>
                            </li>
                            <li class="header__menu-item">
                                <a class="header__menu-link" href="stock.html">Акции</a>
                            </li>
                            <li class="header__menu-item">
                                <a class="header__menu-link" href="contact.html">Контакты</a>
                            </li>
                        </ul>
                    </nav>

                    <div class="header-contact">
                        <div class="header-contact__phone">
                            <p class="header-contact__tel">
                                <a href="tel:+7495327-81-20">+7 (495) 327-81-20</a>
                            </p>
                        </div>
                        
                        <a href="#myModal" title="Заказать звонок">
                            <span class="btn header-contact__btn" role="button">
                                <svg class="icon header__icon">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                                Заказать звонок
                            </span>
                        </a>
                    </div>

                    <div class="header-basket">
                        <a href="#" class="header-basket__link">
                            <svg class="icon header-basket__icon">
                                <use xlink:href="#icon-basket"></use>
                            </svg>

                            Ваша корзина
                            <span class="header-basket__sum">5</span>
                        </a>
                        <p class="header-basket__price">
                            на сумму 6860 Р
                        </p>
                    </div>
                </div>
            </header>

            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">Каталог товаров</h1>
                    </header>
                    
                <?php foreach ($arGoods as $key => $item):?>

                    <?php if($key % 6 == 0): ?>

                        <?php
                            $reverse = '';
                            if (intval($key / 6) % 2 == 1) {
                                $reverse = 'flex-reverse';
                            }
                        ?>
                    
                    <section class="main__inner <?= $reverse ?>">
                        <div class="main-product">
                    <?php endif; ?>
                            <div class="main-product__item">
                                <div class="main-product__image">
                                    <img src="_assets/img/material/<?= $item['IMG'] ?>" alt="Enteley" class="main-product__img"/>

                                    <p class="main-product__price"><?= $item['PRICE'] ?> Р</p>
                                </div>

                                <div class="main-product__description">
                                    <span class="btn main-product__btn" role="button">
                                        <svg class="icon header__icon">
                                            <use xlink:href="#icon-basket"></use>
                                        </svg>
                                        Купить товар
                                    </span>

                                    <div class="main-product_detail">
                                        <p>Код: <?= $item['CODE'] ?></p>
                                        <p>Производитель: <?= $item['PRODUCER'] ?></p>
                                        <p>Состав: <?= $item['CONSIST'] ?></p>
                                    </div>
                                </div>
                            </div>                      
                        
                    <?php if ($key % 6 == 5): ?>
                        </div>

                        <div class="main-fashion">
                            <img src="_assets/img/slide/<?= $arPreviewImages[intval($key / 6)] ?>" alt="Enteley" class="main-fashion__img"/>
                        </div>
                    </section>
                    <?php endif; ?>
                <?php endforeach; ?>
                </section>    
            </main>

            <footer class="footer">
                <div class="footer__inner inner">
                    <div class="footer-copy">
                        <p class="footer-copy__inner">
                            &copy; 2018 &laquoEnteley&raquo <br> Интернет-магазин тканей
                        </p>
                    </div>

                    <div class="footer-contact">
                        <div class="footer-contact__vcard vcard">
                            <div class="footer-contact__phone">
                                <span class="footer-contact__tel tel">
                                    <a href="tel:+7495327-81-20">+7 (495) 327-81-20</a>
                                </span>
                            </div>

                            <div class="footer-contact__adr adr">
                                <span class="footer-contact__locality locality">г. Москва, М. Станкостроительная</span><br>
                                <span class="footer-contact__street-address street-address">ул. Станкостроителей, д. 25</span>
                            </div>

                            <p class="footer-contact__email">
                                <a class="email" href="mailto:example@domain.com">Написать письмо</a>
                            </p>
                        </div>
                    </div>

                    <nav class="footer__menu">
                        <ul class="footer__menu-inner">
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="about.html">О компании</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="index.html">Каталог товаров</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="pravila.html">Памятка покупателю</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="oplata-dostavka.html">Оплата и доставка</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="stock.html">Акции</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="contact.html">Контакты</a>
                            </li>
                        </ul>
                    </nav>

                    <div class="footer-share">
                        <p class="footer-share__item">
                            Поделись с друзьями
                        </p>

                        <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,twitter" data-counter=""></div>
                    </div>
                </div>
            </footer>
        </div>
        
        <div id="myModal" class="modal">
            <div class="modal__dialog">
                <div class="modal__content">
                    <div class="modal__header">
                        <h3 class="modal__title">Заказать звонок</h3>
                        <a href="#close" title="Закрыть" class="modal__close">×</a>
                    </div>
                    <div class="modal__body">
                        <form action="" class="modal__form" method="get">
                            <fieldset class="modal__form-fieldset">
                                <legend class="modal__form-legend">Контактная информация</legend>
                                <p class="modal__form-inner">
                                    <label class="modal__form-label" for="name">Имя <em>*</em></label>
                                    <input type="text" class="modal__form-input" id="name" name="name" placeholder="Введите имя" required>
                                </p>
                                <p class="modal__form-inner">
                                    <label class="modal__form-label" for="tel">Телефон <em>*</em></label>
                                    <input type="tel" class="modal__form-input" id="tel" name="tel" placeholder="Введите номер телефона" required>
                                </p>
                                <p class="modal__form-inner">
                                    <input class="btn__lr modal__form-btn" type="submit" value="Отправить">
                                </p>
                            </fieldset>
                        </form>
                    </div>
                    <div class="modal__footer">
                        <a href="#close" title="Закрыть" class="btn modal__close">Закрыть</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="hidden">
            <?php 
                $file = file_get_contents(__DIR__.'/_assets/svg/sprite.svg'); 
                echo $file;
            ?>
        </div>
        
        <script src="http://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="http://yastatic.net/share2/share.js"></script>
    </body>
</html>
